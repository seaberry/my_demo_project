/*****************************************************************************/
/**
 * @file 
 *   demo.c
 * @brief 
 *   My demo project
 * @par
 *   Used to demonstrate  Atlassian tool suite
 *
 *****************************************************************************/

/*
 * Copyright (c) 2021 Occuity Ltd. All rights reserved.
 */

/*****************************************************************************/
/* Included Files                                                            */
/*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>

#include "demo.h"

/*****************************************************************************/
/* Definitions                                                               */
/*****************************************************************************/

#define MY_SIGNED_NUMBER                (5)
#define MY_UNSIGNED_NUMBER              (5u)

/*****************************************************************************/
/* Type definitions                                                          */
/*****************************************************************************/

typedef enum
{
    MY_ENUM_A = 10u,
    MY_ENUM_B = 20u
} my_enum_t;

typedef struct my_structure
{
    char element_1;
    char element_2;
} my_structure_t;

/*****************************************************************************/
/* Private (file scope only) variables                                       */
/*****************************************************************************/

static uint8_t g_private_variable = 0;
static uint8_t g_private_variable_initialized = MY_UNSIGNED_NUMBER;

/*****************************************************************************/
/* Public (external) variables                                               */
/*****************************************************************************/

uint8_t g_demo_public_variable = MY_UNSIGNED_NUMBER;

/*****************************************************************************/
/* Local functions                                                           */
/*****************************************************************************/

static void my_local_function(void);
static void my_pointer_out(uint8_t* const p_data);
static void my_pointer_in(const uint8_t* const p_data);
static void my_if_else_conditions(void);
static void my_switch_statement(my_enum_t my_enum);
static void my_for_loop(void);
static void my_while_loop(void);
static void my_do_while_loop(void);

/**
 * @brief 
 *   Description of function's purpose
 */
static void my_local_function(void)
{
    my_private_variable++;
}

/**
 * @brief
 *   Description of function’s purpose
 * @param[out]
 *   p_data Description of parameter's purpose
 */
static void my_pointer_out(uint8_t* const p_data)
{
    *p_data = g_private_variable;
}

/**
 * @brief
 *   Description of function’s purpose
 * @param[in]
 *   p_data Description of parameter's purpose
 */
static void my_pointer_in(const uint8_t* const p_data)
{
    g_private_variable = *p_data;
}

/**
 * @brief
 *   Description of function’s purpose
 * @param[out]
 *   p_data Description of parameter's purpose
 */
static void my_pointer_out(uint8_t* const p_data)
{
    *p_data = g_private_variable;
}

/**
 * @brief 
 *   Examples of if & else in both run-time code and pre-compiler conditions
 */
static void my_if_else_conditions(void)
{
#if defined(IF_ONLY)
    if (my_private_variable < MY_UNSIGNED_NUMBER)
    {
        /// @todo Add code
    }
#else 
#if defined(IF_ELSE_ONLY)
    if (my_private_variable < MY_UNSIGNED_NUMBER)
    {
        /// @todo Add code
    }
    else
    {
        /// @todo Add code
    }
#else
    if (my_private_variable == MY_UNSIGNED_NUMBER)
    {
        /// @todo Add code
    }
    else if (my_private_variable < MY_UNSIGNED_NUMBER)
    {
        /// @todo Add code
    }
    else
    {
        /// @todo Add code (or comment, if there is nothing to do)
    }
#endif /* !defined(IF_ELSE_ONLY) */
#endif /* !defined(IF_ONLY) */
}

/**
 * @brief 
 *   Example of switch statement
 * @param
 *   my_enum Description of parameter's purpose
 */
static void my_switch_statement(my_enum_t my_enum)
{
    switch (my_enum)
    {
        case MY_ENUM_A:
            /// @todo Add code
        break;

        case MY_ENUM_B:
            /// @todo Add code
        break;

        default:
	        /// @todo Add code (or comment, if there is nothing to do)
        break;
    }
}

/**
 * @brief 
 *   Example of for loop
 */
static void my_for_loop(void)
{
    uint8_t index;

    for (index = 0; index < MY_UNSIGNED_NUMBER; ++index)
    {
        /// @todo Add loop body
    }
}

/**
 * @brief 
 *   Example of while loop
 */
static void my_while_loop(void)
{
    while (my_private_variable <= MY_UNSIGNED_NUMBER)
    {
        /// @todo Add code
		my_private_variable++;
    }
}

/**
 * @brief 
 *   Example of do while loop
 */
static void my_do_while_loop(void)
{
    do
    {
        /// @todo Add code
		my_private_variable++;
    } while (my_private_variable <= MY_UNSIGNED_NUMBER);
}

/*****************************************************************************/
/* External interface functions                                              */
/*****************************************************************************/

/**
 * @brief
 *   Description of function’s purpose
 * @param[in]
 *   parameter_1 Description of parameter's purpose
 * @param[in]
 *   parameter_2 Description of parameter's purpose
 * @return 
 *   Descripton of returned data
 */
uint8_t DEMO_external_function(
    uint8_t const parameter_1, 
	uint8_t const parameter_2)
{
    // Comment as appropriate
    if ((my_parameter_1 > 0) && (my_parameter_2 < MY_UNSIGNED_NUMBER))
    {
        my_local_function();
    }

    return my_private_variable;
}


/*** end of file ***/
