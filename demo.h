/*****************************************************************************/
/**
 * @file 
 *   demo.h
 * @brief 
 *   My demo project
 * @par
 *   Used to demonstrate  Atlassian tool suite
 *
 *****************************************************************************/

/*
 * Copyright (c) 2021 Occuity Ltd. All rights reserved.
 */

#if !defined(DEMO_H)
#define DEMO_H

/*****************************************************************************/
/* Definitions                                                               */
/*****************************************************************************/

#define DEMO_MAGIC_NUMBER               (1u)

/*****************************************************************************/
/* Type definitions                                                          */
/*****************************************************************************/

typedef enum
{
    DEMO_ALPHA,
    DEMO_BETA
} demo_enum_t;

typedef struct demo_structure
{
    char element_1;
    char element_2;
} demo_structure_t;

/*****************************************************************************/
/* Public (external) variables                                               */
/*****************************************************************************/

extern uint8_t g_demo_public_variable;

/*****************************************************************************/
/* Function prototypes                                                       */
/*****************************************************************************/

uint8_t DEMO_external_function(
    uint8_t const parameter_1, 
	uint8_t const parameter_2);

#endif // defined(DEMO_H)


/*** end of file ***/
